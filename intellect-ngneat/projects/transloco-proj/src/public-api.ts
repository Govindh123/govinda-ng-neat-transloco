/*
 * Public API Surface of transloco-proj
 */

export * from './lib/transloco-proj.service';
export * from './lib/transloco-proj.component';
export * from './lib/transloco-proj.module';
