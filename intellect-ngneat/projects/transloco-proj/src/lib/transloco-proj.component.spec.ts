import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TranslocoProjComponent } from './transloco-proj.component';

describe('TranslocoProjComponent', () => {
  let component: TranslocoProjComponent;
  let fixture: ComponentFixture<TranslocoProjComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TranslocoProjComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TranslocoProjComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
