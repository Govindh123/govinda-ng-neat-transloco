import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'lib-transloco-proj',
  template: `
  <p>
  {{'scopeName.HELLO' | transloco}}
</p>
  `,
  styles: [
  ]
})
export class TranslocoProjComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
