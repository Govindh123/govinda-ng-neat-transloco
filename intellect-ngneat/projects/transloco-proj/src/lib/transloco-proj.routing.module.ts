import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TranslocoProjComponent } from './transloco-proj.component';

const routes: Routes = [
  {
    path: '',
    component: TranslocoProjComponent
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class TranslocoProjRoutingModule {}
