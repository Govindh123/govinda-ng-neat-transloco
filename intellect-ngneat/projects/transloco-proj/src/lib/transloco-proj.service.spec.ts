import { TestBed } from '@angular/core/testing';

import { TranslocoProjService } from './transloco-proj.service';

describe('TranslocoProjService', () => {
  let service: TranslocoProjService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TranslocoProjService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
