import { NgModule } from '@angular/core';
import { TranslocoModule, TRANSLOCO_SCOPE } from '@ngneat/transloco';
import { TranslocoProjComponent } from './transloco-proj.component';
import { TranslocoProjRoutingModule } from './transloco-proj.routing.module';

export const loader = ['en'].reduce((acc: any, lang: any):any => {
  acc[lang] = () => import(`./assets/i18n/${lang}.json`);
  return acc;
}, {});

@NgModule({
  declarations: [
    TranslocoProjComponent
  ],
  imports: [
    TranslocoProjRoutingModule,
    TranslocoModule
  ],
  exports: [
    TranslocoProjComponent
  ],
  providers: [
    {
      provide: TRANSLOCO_SCOPE,
      useValue: {
        scope: 'scopeName',
        loader
      }
    }
  ],
})
export class TranslocoProjModule { }
